<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
    <UiMod name="AuctionTweaker" version="1.1" date="12/15/2008" >
        <Author name="Felyza" email="" />
        <VersionSettings gameVersion="1.3.3" windowsVersion="1.0" savedVariablesVersion="1.0" />
        <Description text="Adds missing options to the auction window." />
        <Dependencies>
            <Dependency name="EA_AuctionHouseWindow" />
        </Dependencies>
        <Files>
            <File name="AuctionTweaker.lua" />
        </Files>
        <OnInitialize>
            <CallFunction name="AuctionTweaker.OnInitialize" />
        </OnInitialize>
        <SavedVariables>
        </SavedVariables>
    </UiMod>

</ModuleFile>
