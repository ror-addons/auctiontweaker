AuctionTweaker = {}

--[[

My one and only setting, changable only here. After testing out different things, I've deduced which
are of even minor, and which are not. For instance, item type Enhancement is identical to Talisman, or
that Parry Skill has been replaced on all items with Parry %. Some effects are GM only, such as gravity
and levitation, and will never appear in the auction house. You can change the value of AddUselessItems
to true, and these types of values will be added to the auction window, but either expect duplicates of
other search parameters or nothing to ever come up for them.

]]

AuctionTweaker.AddUselessItems = false

function AuctionTweaker.OnInitialize()
   AuctionTweaker.AddItemTypes()   
   AuctionTweaker.AddItemSlots()   
   AuctionTweaker.AddItemStats()  
   AuctionTweaker.DefaultClear = AuctionSearchWindow.Clear
   AuctionSearchWindow.Clear   = AuctionTweaker.Clear
end

function AuctionTweaker.Clear()
   local windowName = "AuctionHouseWindowCreateSearch"
   TextEditBoxSetText( windowName.."MinRank", AuctionSearchWindow.rankNotSetText )
   TextEditBoxSetText( windowName.."MaxRank", AuctionSearchWindow.rankNotSetText )
   AuctionTweaker.DefaultClear()
end

function AuctionTweaker.AddItemTypes()
   table.insert(AuctionSearchWindow.ItemTypeChoice, {value=22, text=L"Accessory", isSelected=false})
   table.insert(AuctionSearchWindow.ItemTypeChoice, {value=24, text=L"Trophy", isSelected=false})
   table.insert(AuctionSearchWindow.ItemTypeChoice, {value=32, text=L"Salvaging", isSelected=false})
   table.insert(AuctionSearchWindow.ItemTypeChoice, {value=21, text=L"Quest Item", isSelected=false})
   if AuctionTweaker.AddUselessItems then 
      table.insert(AuctionSearchWindow.ItemTypeChoice, {value=23, text=L"Enhancement", isSelected=false})
      table.insert(AuctionSearchWindow.ItemTypeChoice, {value=29, text=L"Basic Mount", isSelected=false}) 
      table.insert(AuctionSearchWindow.ItemTypeChoice, {value=30, text=L"Advanced Mount", isSelected=false}) 
      table.insert(AuctionSearchWindow.ItemTypeChoice, {value=33, text=L"Marketing", isSelected=false}) 
   end
end

function AuctionTweaker.AddItemSlots()
   table.insert(AuctionSearchWindow.DefaultSlots, {value=11, text=L"Pocket", isSelected=false})
   table.insert(AuctionSearchWindow.DefaultSlots, {value=5,  text=L"Banner", isSelected=false})
end

function AuctionTweaker.AddItemStats()
   table.insert(AuctionSearchWindow.StatModChoice, {value=26, text=L"Armor", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=28, text=L"Block %", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=29, text=L"Parry %", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=30, text=L"Dodge %", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=31, text=L"Disrupt %", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=47, text=L"Damage Shield", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=83, text=L"Armor Pen Reduction", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=84, text=L"Crit Rate Reduction", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=67, text=L"Hate Caused", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=68, text=L"Hate Reduction", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=80, text=L"Melee Power", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=76, text=L"Melee Crit Rate", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=81, text=L"Range Power", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=77, text=L"Range Crit Rate", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=82, text=L"Magic Power", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=78, text=L"Magic Crit Rate", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=94, text=L"Healing Power", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=89, text=L"Healing Crit Rate", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=85, text=L"Block Strikethrough", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=86, text=L"Parry Strikethrough", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=87, text=L"Evade Strikethrough", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=88, text=L"Disrupt Strikethrough", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=79, text=L"HP Regen", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=32, text=L"AP Regen", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=33, text=L"Morale Regen", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=90, text=L"Max AP", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=59, text=L"Butchering", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=60, text=L"Scavenging", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=61, text=L"Cultivation", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=62, text=L"Apothecary", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=63, text=L"Talisman Making", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=64, text=L"Salvaging", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=38, text=L"AutoAttack Speed", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=37, text=L"Range", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=65, text=L"Stealth", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=66, text=L"Stealth Detection", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=69, text=L"Offhand Chance", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=70, text=L"Offhand Damage", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=42, text=L"Crit Rate", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=36, text=L"Crit Damage", isSelected=false})
   table.insert(AuctionSearchWindow.StatModChoice, {value=73, text=L"Dismount Chance", isSelected=false})
   if AuctionTweaker.AddUselessItems then 
      table.insert(AuctionSearchWindow.StatModChoice, {value=10, text=L"Block Skill", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=11, text=L"Parry Skill", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=12, text=L"Dodge Skill", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=13, text=L"Disrupt Skill", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=22, text=L"Inc Damage", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=23, text=L"Inc Damage %", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=24, text=L"Out Damage", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=25, text=L"Out Damage %", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=27, text=L"Velocity", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=34, text=L"Cooldown", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=35, text=L"Build Time", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=44, text=L"Effect Resist", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=45, text=L"Effect Buff", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=48, text=L"Setback Chance", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=49, text=L"Setback Value", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=50, text=L"XP Worth", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=51, text=L"RP Worth", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=52, text=L"Inf Worth", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=53, text=L"Gold Worth", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=54, text=L"Aggro Radius", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=55, text=L"Target Duration", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=56, text=L"Spec", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=57, text=L"Gold Looted", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=58, text=L"XP Received", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=74, text=L"Gravity", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=75, text=L"Levitation Height", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=91, text=L"Specline 1", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=92, text=L"Specline 2", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=93, text=L"Specline 3", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=95, text=L"Interact Time", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=71, text=L"RP Received", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=72, text=L"Inf Received", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=43, text=L"Crit Damage Taken", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=46, text=L"Minimum Range", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=41, text=L"AP Cost", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=39, text=L"Radius", isSelected=false})
      table.insert(AuctionSearchWindow.StatModChoice, {value=40, text=L"AutoAttack Damage", isSelected=false})
   end      
end



